document.addEventListener("scroll", function (e) {
  var top = window.pageYOffset + window.innerHeight,
    isVisibleImgCards = top > document.querySelector("#imgCards").offsetTop,
    isVisibleMeetPartnerts =
      top > document.querySelector("#partner > div").offsetTop;

  if (isVisibleImgCards) {
    document.getElementById("imgCards").classList.add("animate-slideUp");
  }
  if (isVisibleMeetPartnerts) {
    document.getElementById("partner").classList.add("animate-slideUp");
  }
});
